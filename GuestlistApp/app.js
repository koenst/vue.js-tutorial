new Vue({
    el:'#app',
    data:{
        event:{
            date: 'August 14th - 16th',
            title: 'Summer Festival!',
            description: "It's back! This years summer festival will be in the beautiful countryside featuring our best line up ever!" 
        },
        newNameText: '',
        guestNames: [],
        eventCapacity: 25,
        eventCapacityPercentage: 0,
    },
    methods: {        
        formSubmitted: function () {
            if (this.newNameText.length > 0 && this.eventCapacityPercentage < 100) {
                this.guestNames.push(this.newNameText)
                this.newNameText = ''
                this.eventCapacityPercentage = this.guestNames.length / this.eventCapacity * 100                
            }
        }        
    },
    //computed is very performant, but for async tasks you need the watch property
    computed: {
        sortNames: function(){
            return this.guestNames.sort()
        }
    },
    watch: {
        guestName: function(data){
            //do something asynchronously
        }
    },
    //filters are intended for simple functionality such as uncomplicated text formatting
    filters: {
        formatName: function(value) {
            return value.slice(0,1).toUpperCase() + value.slice(1).toLowerCase()
        }
    }
});

new Vue({
    el: '#nav',
    data: {
        appName: 'Guest List',
        navLinks: [
            {name: "Home", id: 1, url: ""},
            {name: "Upcoming events", id: 2, url: ""},
            {name: "Latest news", id: 3, url: ""},
        ]
    },
});